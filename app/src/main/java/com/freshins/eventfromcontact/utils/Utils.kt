package com.freshins.eventfromcontact.utils

fun getMonthByNumber(number: Int): String? {
    val month = listOf(
        "Января", "Февраля", "Марта", "Апреля", "Мая", "Июня", "Июля", "Августа", "Сентября"
        , "Октября", "Ноября", "Декабря"
    )
    return if (number !in 1..12) {
        null
    } else {
        month[number - 1]
    }
}
package com.freshins.eventfromcontact

import android.app.Application
import com.freshins.eventfromcontact.model.Repository

class ContatsEventApp: Application() {


    override fun onCreate() {
        super.onCreate()
        Repository.initRepository(this)
    }
}
package com.freshins.eventfromcontact.model.dao

import androidx.room.*
import com.freshins.eventfromcontact.model.entity.EventContact

@Dao
interface DaoEventContacts {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertEventContact(EventContact: EventContact)

    @Query("SELECT * FROM eventcontact")
    fun getAllEvents(): List<EventContact>

}



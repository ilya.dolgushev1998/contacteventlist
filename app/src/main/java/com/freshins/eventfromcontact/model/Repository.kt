package com.freshins.eventfromcontact.model

import android.annotation.SuppressLint
import android.content.Context
import com.freshins.eventfromcontact.model.entity.EventContact

@SuppressLint("StaticFieldLeak")
object Repository {

    lateinit var context: Context

    fun initRepository(context: Context) {
        this.context = context
    }

    fun insertEventContacts(eventContact: List<EventContact>) {
        for (event in eventContact) {
            AppDatabase.getAppDataBase(context)?.eventContactsDao()!!.insertEventContact(event)
        }
    }

    fun getAllEvents(): List<EventContact> {
        return AppDatabase.getAppDataBase(context)?.eventContactsDao()!!.getAllEvents()
    }

}
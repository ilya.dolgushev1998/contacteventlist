package com.freshins.eventfromcontact.model.entity

import androidx.room.*
import com.freshins.eventfromcontact.model.converters.ConverterDate
import com.freshins.eventfromcontact.utils.getMonthByNumber
import java.text.SimpleDateFormat
import java.util.*

@TypeConverters(ConverterDate::class)
@Entity
data class EventContact(
    @PrimaryKey val id: String,
    @ColumnInfo(name = "name")val name: String = "",
    @ColumnInfo(name = "eventName")val eventName: String = "",
    @ColumnInfo(name = "date") val date: Date = Date(),
    @ColumnInfo(name = "photoUri")val photoUri: String = "",
    @ColumnInfo(name = "numberPhone")val numberPhone: String = ""
) {
    fun getDataString(): String {
        val day = SimpleDateFormat("dd", Locale.getDefault()).format(date)
        val month = SimpleDateFormat("MM", Locale.getDefault()).format(date)
        return "$day ${getMonthByNumber(month.toInt())}"
    }
}
